#include<stdio.h>
#include<conio.h>
void main()
{
	int a[3][3], b[3][3], c[3][3]={0}, d[3][3]={0};
	int i,j,k,s,t,u,v;
	printf("Enter no. of rows and columns in matrix A: ");
	scanf("%d%d",&s,&t);
	printf("Enter no. of rows and columns in matrix B: ");
	scanf("%d%d",&u,&v);
	if(s!=u || t!=v)
	{
		printf("Matrix Addition is not possible");
		return;
	}
	else if(t!=u)
	{
		printf("Matrix Multiplication is not possible");
		return;
	}
	else
	{
		printf("Enter elements of matrix A: ");
 		for(i=0;i<s;i++)
			for(j=0;j<t;j++)
				scanf("%d", &a[i][j]);
		printf("Enter elements of matrix B: ");
		for(i=0;i<u;i++)
			for(j=0;j<v;j++)
				scanf("%d", &b[i][j]);
		//Matrix Addition
		for(i=0;i<s;i++)
			for(j=0;j<t;j++)
				c[i][j] = a[i][j] + b[i][j];
		printf("\nResult of Matrix Addition:\n");
		for(i=0;i<s;i++)
		{
			for(j=0;j<t;j++)
				printf("%d ", c[i][j]);
			printf("\n");
		}
		//Matrix Multiplication
		for(i=0;i<s;i++)
			for(j=0;j<v;j++)
				for(k=0;k<u;k++)
					d[i][j] += a[i][k]*b[k][j];
		printf("\nResult of Matrix Multiplication:\n");
		for(i=0;i<s;i++)
		{
			for(j=0;j<v;j++)
				printf("%d ", d[i][j]);
			printf("\n");
		}
	}
	getch();
}

